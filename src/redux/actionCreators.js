import * as actions from './actionTypes'

const togglePanel = (panel) => {
    return {
        type: actions.TOGGLE_PANEL,
        panel: panel
    }
}

const addStrategy = (name, url, metadata) => {
    return {
        type: actions.ADD_STRATEGY,
        strategy: {
            name: name,
            url: url, 
            metadata: metadata
        }
    }
}

const changeObjectSize = (objectSize) => {
    return {
        type: actions.CHANGE_OBJECT_SIZE,
        objectSize: objectSize
    }
}

const changeLineColor = (color) => {
    return {
        type: actions.CHANGE_LINE_COLOR,
        color: color
    }
}

const changeLineType = (lineType) => {
    return {
        type: actions.CHANGE_LINE_TYPE,
        lineType: lineType
    }
}

const toggleDrawing = (boolean) => {
    return {
        type: actions.TOGGLE_DRAWING,
        boolean: boolean
    }
}

const createLine = (currentLineColor, currentLineType) => {
    return {
        type: actions.CREATE_LINE,
        line: {
            linePoints: [],
            lineColor: currentLineColor,
            lineType: currentLineType
        }
    }
}

const setLine = (x, y) => {
    return {
        type: actions.SET_LINE,
        x: x,
        y: y,
    }
}

const restoreHistory = (history, historyIndex) => {
    return {
        type: actions.RESTORE_HISTORY,
        history: history,
        historyIndex: historyIndex
    }
}

const undo = () => {
    return {
        type: actions.UNDO,
    }
}

const redo = () => {
    return {
        type: actions.REDO
    }
}

const cleanCanvas = () => {
    return {
        type: actions.CLEAN_CANVAS
    }
}

const toggleRecicleByn = (boolean) => {
    return {
        type: actions.TOGGLE_RECICLE_BYN,
        boolean: boolean
    }
}

const setEraser = (boolean) => {
    return {
        type: actions.SET_ERASER,
        boolean: boolean
    }
}

const changeBackground = (background) => {
    return {
        type: actions.CHANGE_BACKGROUND,
        background: background
    }
}

const changeBackgroundAngle = (backgroundAngle) => {
    return {
        type: actions.CHANGE_BACKGROUND_ANGLE,
        backgroundAngle: backgroundAngle
    }
}

const toggleDefaultPlayersBlue = (playersType) => {
    return {
        type: actions.TOGGLE_DEFAULT_PLAYERS_BLUE,
        playersType: playersType
    }
}

const toggleDefaultPlayersRed = (playersType) => {
    return {
        type: actions.TOGGLE_DEFAULT_PLAYERS_RED,
        playersType: playersType
    }
}

const toggleDefaultObjects = (objectType) => {
    return {
        type: actions.TOGGLE_DEFAULT_OBJECTS,
        objectType: objectType
    }
}

// const addAttackCustomTeam = (customTeam) => {
//     return {
//         type: actions.ADD_ATTACK_CUSTOM_TEAM,
//         customTeam: customTeam
//     }
// }

// const addDefenseCustomTeam = (customTeam) => {
//     return {
//         type: actions.ADD_DEFENSE_CUSTOM_TEAM,
//         customTeam: customTeam
//     }
// }

const addCustomTeam = (teamSelected, customTeam) => {
    return {
        type: actions.ADD_CUSTOM_TEAM,
        teamSelected: teamSelected,
        customTeam: customTeam
    }
}

const addSelectedCustomTeam = (team) => {
    return {
        type: actions.ADD_SELECTED_CUSTOM_TEAM,
        team: team
    }
}

const addMyTeam = (team) => {
    return {
        type: actions.ADD_MY_TEAM,
        team: team
    }
}

const updateObjectPosition = (objectType, typeId, elementId, x, y) => {
    return {
        type: actions.UPDATE_OBJECT_POSITION,
        objectType: objectType,
        typeId: typeId,
        elementId: elementId,
        x: x,
        y: y,
    }
}

const updateCustomPlayerPosition = (objectType, elementId, x, y) => {
    return {
        type: actions.UPDATE_CUSTOM_PLAYER_POSITION,
        objectType: objectType,
        elementId: elementId,
        x: x,
        y: y
    }
}

const setUser = (currentUser) => {
    return {
        type: actions.SET_USER,
        currentUser: currentUser
    }
}

const setConfigData = (configData) => {
    return {
        type: actions.SET_CONFIG_DATA,
        configData: configData
    }
}

export { 
    toggleDefaultPlayersBlue, 
    toggleDefaultPlayersRed, 
    toggleDefaultObjects, 
    addCustomTeam,
    addSelectedCustomTeam,
    addMyTeam,
    updateObjectPosition,
    updateCustomPlayerPosition,
    togglePanel, 
    addStrategy, 
    setUser, 
    setConfigData,
    changeObjectSize,
    toggleDrawing, 
    changeLineColor, 
    changeLineType, 
    createLine, 
    setLine, 
    restoreHistory,
    undo, 
    redo,
    cleanCanvas, 
    setEraser,
    toggleRecicleByn,
    changeBackground, 
    changeBackgroundAngle
}

