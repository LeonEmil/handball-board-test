const canvas = document.querySelector("canvas")
let c = canvas ? canvas.getContext("2d") : undefined

let mouse = {
    x: undefined,
    y: undefined
}

window.addEventListener("mousemove", e => {
    mouse.x = e.x
    mouse.y = e.y
    console.log(e.x, e.y)
})

if(c && canvas){
    const animation = () => {
        //c.beginPath()
    
        c.beginPath()
        c.arc(mouse.x - 100, mouse.y, 5, 0, Math.PI * 2)
        c.strokeStyle = "red"
        c.fill()
        c.closePath()
        //c.closePath()
        console.log("a")
    }
    console.log(window.requestAnimationFrame(animation))
    window.requestAnimationFrame(animation)
}