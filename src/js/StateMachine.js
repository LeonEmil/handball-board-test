

const drawMachine = {
    start: {
        ONMOUSEDOWN: 'lineCreation'
    },
    lineCreation: {
        ONMOUSEMOVE: 'lineUpdate'
    },
    lineUpdate: {
        ONMOUSEUP: 'lineFinish'
    }
}

const drawState = {
    currentState: 'start',
    action: '',
    lines: []
}

const transition = (action) => {
    const currentDrawState = drawState.currentState
    const nextDrawState = drawMachine[currentDrawState][action.type]
    if(nextDrawState){
        
    }
}
