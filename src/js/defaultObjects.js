const section = (window.innerHeight - 60) / 13

const defaultObjects = [
    [
        {
            position: {
                x: 30,
                y: section
            }
        },
    ],
    [
        {
            position: {
                x: 30,
                y: section * 2
            }
        },
    ],
    [
        {
            position: {
                x: 30,
                y: section * 3
            }
        },
    ],
    [
        {
            position: {
                x: 30,
                y: section * 4
            }
        },
    ],
    [
        {
            position: {
                x: 30,
                y: section * 5
            }
        },
    ],
    [
        {
            position: {
                x: 30,
                y: section * 6
            }
        },
    ],
    [
        {
            position: {
                x: 30,
                y: section * 7
            }
        },
    ],
    [
        {
            position: {
                x: 30,
                y: section * 8
            }
        },
    ],
    [
        {
            position: {
                x: 30,
                y: section * 9
            }
        },
    ],
    [
        {
            position: {
                x: 30,
                y: section * 10
            }
        },
    ],
    [
        {
            position: {
                x: 30,
                y: section * 11
            }
        },
    ],
    [
        {
            position: {
                x: 30,
                y: section * 12
            }
        },
    ],
    [
        {
            position: {
                x: 30,
                y: section * 13
            }
        },
    ],
    [
        {
            position: {
                x: window.innerWidth - 120,
                y: section
            }
        },
    ],
    [
        {
            position: {
                x: window.innerWidth - 120,
                y: section * 2
            }
        },
    ],
    [
        {
            position: {
                x: window.innerWidth - 120,
                y: section * 3
            }
        },
    ],
    [
        {
            position: {
                x: window.innerWidth - 120,
                y: section * 4
            }
        },
    ],
    [
        {
            position: {
                x: window.innerWidth - 120,
                y: section * 5
            }
        },
    ],
    [
        {
            position: {
                x: window.innerWidth - 120,
                y: section * 6
            }
        },
    ],
    [
        {
            position: {
                x: window.innerWidth - 120,
                y: section * 7
            }
        },
    ],
    [
        {
            position: {
                x: window.innerWidth - 120,
                y: section * 8
            }
        }
    ],
    [
        {
            position: {
                x: window.innerWidth - 120,
                y: section * 9
            }
        }
    ],
    [
        {
            position: {
                x: window.innerWidth - 120,
                y: section * 10
            }
        }
    ],
    [
        {
            position: {
                x: window.innerWidth - 120,
                y: section * 11
            }
        }
    ],
    [
        {
            position: {
                x: window.innerWidth - 120,
                y: section * 12
            }
        }
    ],
]

export default defaultObjects