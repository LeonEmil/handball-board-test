import React from 'react';

const StrategyList = ({strategyItems}) => {
    strategyItems.map((item, key) => {
        return strategyItems.length > 0 ? (  
            <li className="strategy-panel__item" onClick={() => { }} key={key}>
                <a href="www.algo.com" target="_blank" rel="noopener noreferrer" className="strategy-panel__item-link">
                    <span className={`draw-panel__item-span-${key + 1}`}>{item.name}</span>
                    <span className={`draw-panel__item-span-${key + 1} text-gray`}>{item.date}</span>
                    <span className={`draw-panel__item-span-${key + 1} text-gray`}>{item.size}</span>
                </a>
            </li>
        ) : "No hay estrategias guardadas"
    })
}
 
export default StrategyList;