import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { connect } from 'react-redux'
import { togglePanel } from '../../redux/actionCreators';

const SendPanel = ({panels, togglePanel, currentUser, menuRef}) => {
    const panelSize = window.innerWidth < 576 ? '-300px' : '-500px'
    return (  
        <div className="send-panel" style={panels[3].showPanel ? { left: `${menuRef.current.offsetWidth}px`, zIndex: 10 } : { left: panelSize }}>
            <div className="draw-panel__exit-container" onClick={() => {togglePanel(3)}}>
                <FontAwesomeIcon icon={faTimes} color="#075f60" size="2x" className="draw-panel__exit-item" />
            </div>
            <ul className="draw-panel__list">
                <p>No hay estrategias guardadas todavia</p>
            </ul>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        currentUser: state.currentUser,
        panels: state.panels
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        togglePanel: (panel) => { dispatch(togglePanel(panel)) }
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(SendPanel);