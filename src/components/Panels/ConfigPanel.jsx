import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { connect } from 'react-redux'
import { db } from './../../firebase/firebase'
import Modal from './Modal'
import { configButton1Active, configButton1Inactive, configButton2Active, configButton2Inactive } from './../../js/images'

import { changeBackground, togglePanel, changeObjectSize, addCustomTeam, addSelectedCustomTeam, addMyTeam } from './../../redux/actionCreators'


let backgroundFormat = [
    {
        event: "/v1602549201/Handball%20manager/canvas-1_rbigwq.jpg"
    },
    {
        event: "/v1605608953/Handball%20manager/cancha4_anaat0.png"
    },
    {
        event: "/v1605608956/Handball%20manager/cancha5_vofblr.png"
    },
    {
        event: "/v1605608955/Handball%20manager/cancha6_lwyjfv.png"
    },
]

let backgroundColor = [
    {
        event: "/v1602549201/Handball%20manager/canvas-1_rbigwq.jpg",
    },
    {
        event: "/v1602548589/Handball%20manager/canvas-2_ao97dx.jpg",
    },
    {
        event: "/v1602548592/Handball%20manager/canvas-3_uowaon.jpg",
    },
]

const ConfigPanel = ({panels, togglePanel, changeBackground, currentUser, changeObjectSize, addCustomTeam, addSelectedCustomTeam, selectedCustomTeam, addMyTeam}) => {
   
    const [panel, setPanel] = useState("General")

    const handleSubmit = (e) => {
        e.preventDefault()
        e.persist()
        db.collection('users').where("email", '==', currentUser.email).get().then(response => {
            const id = response.docs[0].id
            let user = response.docs[0].data()
            let teamId
            for(let i = 0; i <= user.customTeams.length; i++){
                if(user.customTeams[i].name === selectedCustomTeam.name){
                    teamId = i
                } 
            }
            for(let i = 0; i < user.customTeams[teamId].players.length; i++){
                user.customTeams[teamId].name = e.target[`name${i + 1}`].value
                user.customTeams[teamId].number = e.target[`number${i + 1}`].value
                user.customTeams[teamId].role = e.target[`role${i + 1}`].value
                user.customTeams[teamId].initial = e.target[`initial${i + 1}`].checked
                user.customTeams[teamId].image = e.target[`image${i + 1}`].value
            }
            addSelectedCustomTeam(user.customTeams[teamId])
            db.collection('users').doc(id).update(user).then(() => {
                alert("Equipo actualizado")
            })
        })        
    }

    const handleMyTeam = (e) => {
        e.preventDefault()
        e.persist()
        db.collection('users').where('email', '==', currentUser.email).get().then(response => {
            const id = response.docs[0].id
            const user = response.docs[0].data()
            const newTeam = currentUser.customTeams.filter(el => el.teamName === e.target.team.value)
            user.myTeam = newTeam[0]
            db.collection('users').doc(id).update(user)
        })
    }

    const addPlayer = () => {
        let index = selectedCustomTeam.players.length
        let lastPlayer = {
            [index]: {
                name: "",
                number: 0,
                role: "",
                initial: undefined,
                image: "",
                position: {
                    x: 0,
                    y: 0
                }
            }
        }
        let newPlayers = Object.assign(selectedCustomTeam.players, lastPlayer)
        selectedCustomTeam.players = newPlayers
    }

    return (  
        <div className="config-panel" style={panels[6].showPanel ? { transform: "scaleX(1)", zIndex: 10 } : { transform: "scaleX(0)" }}>
            <div className="draw-panel__exit-container" onClick={() => {togglePanel(6)}}>
                <FontAwesomeIcon icon={faTimes} color="#075f60" size="2x" className="draw-panel__exit-item" />
            </div>
            <div className="config-panel__buttons-container">
                <button 
                    className="config-panel__button-1" 
                    style={
                        panel === "General" ? {backgroundImage: `url(${configButton1Active})`}
                                            : { backgroundImage: `url(${configButton1Inactive})`}
                    } 
                    onClick={() => {setPanel("General")}}
                >
                    General
                </button>
                <button 
                    className="config-panel__button-2" 
                    style={
                        panel === "Equipos" ? { backgroundImage: `url(${configButton2Active})`}
                                            : { backgroundImage: `url(${configButton2Inactive})`}
                    } 
                    onClick={() => {setPanel("Equipos")}}
                >
                    Equipos
                </button>
            </div>
            <div className="config-panel__general" style={panel === "General" ? {display: "block"} : {display: "none"}}>
                <h2 className="config-panel__subtitle">Formato de pista</h2>
                <ul className="config-panel__list">
                    {
                        backgroundFormat.map((image, key) => {
                            return (
                                <li className="config-panel__item" onClick={() => { changeBackground(image.event) }} key={key}>
                                    <div className={`config-panel__item-image-${key + 1}`}></div>
                                </li>
                            )
                        })
                    }
                </ul>
                <h2 className="config-panel__subtitle">Color de pista</h2>
                <ul className="config-panel__list">
                    {
                        backgroundColor.map((image, key) => {
                            return (
                                <li className="config-panel__item" onClick={() => { changeBackground(image.event) }} key={key}>
                                    <div className={`config-panel__item-image-color-${key + 1}`}></div>
                                </li>
                            )
                        })
                    }
                </ul>
                <h2 className="config-panel__subtitle">Jugadores</h2>
                <ul>

                </ul>
                <h2 className="config-panel__subtitle">Tamaño</h2>
                <ul className="config-panel__size-list">
                    <li className="config-panel__size-item" onClick={() => { changeObjectSize("normal") } }>
                        <div className="config-panel__size-item">Normal</div>
                    </li>
                    <li className="config-panel__size-item" onClick={() => { changeObjectSize("medium") } }>
                        <div className="config-panel__size-item">Mediano</div>
                    </li>
                    <li className="config-panel__size-item" onClick={() => { changeObjectSize("small") } }>
                        <div className="config-panel__size-item">Pequeño</div>
                    </li>
                </ul>
            </div>

            {/* ---------------------------------- Panel de configuracion de equipos --------------------------- */}

            <div className="config-panel__teams" style={panel === "Equipos" ? { display: "block" } : { display: "none" }}>
                <h2 className="config-panel__subtitle">Gestionar equipos</h2>
                <Modal />

                <h2 className="config-panel__subtitle">Mi equipo</h2>
                {
                    currentUser.myTeam ? 
                        <div className="config-panel__my-team">
                            <span>{currentUser.myTeam.teamName}</span>
                            <button className="config-panel__teams-button" onClick={() => { addSelectedCustomTeam(currentUser.myTeam) }}>Editar equipo</button>
                            <button className="config-panel__teams-button" onClick={() => { addCustomTeam("customAttackTeam", currentUser.myTeam) }}>Equipo atacante</button> 
                            <button className="config-panel__teams-button" onClick={() => { addCustomTeam("customDefenseTeam", currentUser.myTeam) }}>Equipo defensor</button>
                        </div>
                    : null
                }
                
                        <>
                        <br/>
                        <p>Elija uno de los equipos creados para configurarlo como mi equipo</p>
                        <form onSubmit={handleMyTeam}>
                            <select name="team" id="">
                                {
                                    currentUser.customTeams.map((team, key) => {
                                        return <option value={team.teamName} key={key}>{team.teamName}</option>
                                    })
                                }
                            </select>
                            <input type="submit" value="Guardar"/>
                        </form>
                        </>
                

                <h2 className="config-panel__subtitle">Lista de equipos creados</h2>
                <ul className="">
                    {
                        currentUser ?
                        currentUser.customTeams.map((team, key) => {
                            return (
                                <li key={key} className="config-panel__teams-item">
                                    <span className="config-panel__teams-title">{team.teamName}</span> 
                                    <button className="config-panel__teams-button" onClick={() => { addSelectedCustomTeam(team) }}>Editar equipo</button>
                                    <button className="config-panel__teams-button" onClick={() => { addCustomTeam("customAttackTeam", team) }}>Equipo atacante</button> 
                                    <button className="config-panel__teams-button" onClick={() => { addCustomTeam("customDefenseTeam", team) }}>Equipo defensor</button>
                                </li>
                            )
                        })
                        : null
                    }
                </ul>
                {
                    selectedCustomTeam ? 
                        <>
                        <h2 className="config-panel__subtitle">Gestionar posiciones</h2>
                        <form className="" onSubmit={handleSubmit}>
                            {
                                Object.values(selectedCustomTeam.players).map((player, key) => {
                                    return (
                                        <>
                                            <label htmlFor={`name${key + 1}`} className="form-teams__label">Nombre del jugador {key + 1}</label>
                                            <input
                                                type="text" 
                                                name={`name${key + 1}`} 
                                                className="form-teams__input"
                                                id={`name${key + 1}`} 
                                                required
                                            />
                                            <label htmlFor={`number${key + 1}`} className="form-teams__label">Número del jugador {key + 1}</label>
                                            <input
                                                type="number" 
                                                name={`number${key + 1}`} 
                                                className="form-teams__input"
                                                id={`number${key + 1}`} 
                                                required
                                            />
                                            <label htmlFor={`role${key + 1}`} className="form-teams__label">Posición del jugador {key + 1}</label>
                                            <select name={`role${key + 1}`} id="" className="form-teams__select">
                                                <option value="portero">Portero</option>
                                                <option value="central">Central</option>
                                                <option value="pivote">Pivote</option>
                                                <option value="extremo-izquierdo">Extremo izquierdo</option>
                                                <option value="extremo-derecho">Extremo derecho</option>
                                                <option value="lateral-izquierdo">Lateral izquierdo</option>
                                                <option value="lateral-derecho">Lateral derecho</option>
                                            </select>
                                            <label htmlFor={`initial${key + 1}`} className="form-teams__label">¿Jugador inicial?</label>
                                            <input
                                                type="checkbox"
                                                name={`initial${key + 1}`} 
                                                id={`initial${key + 1}`} 
                                                className="form-teams__checkbox"
                                            />
                                            <label htmlFor={`image${key + 1}`} className="form-teams__label">Forma y color</label>
                                            <select name={`image${key + 1}`} id="" className="form-teams__select">
                                                <option value="circle-blue">Círculo azul</option>
                                                <option value="square-blue">Cuadrado azul</option>
                                                <option value="triangle-blue">Triangulo azul</option>
                                                <option value="player-blue-1">Jugador azul (con brazo derecho)</option>
                                                <option value="player-blue-2">Jugador azul (con brazo izquierdo)</option>
                                                <option value="player-blue-with-ball-1">Jugador azul (con pelota en brazo derecho)</option>
                                                <option value="player-blue-with-ball-2">Jugador azul (con pelota en brazo izquierdo)</option>
                                                <option value="circle-red">Círculo rojo</option>
                                                <option value="square-red">Cuadrado rojo</option>
                                                <option value="triangle-red">Triangulo rojo</option>
                                                <option value="player-red-1">Jugador rojo (con brazo derecho)</option>
                                                <option value="player-red-2">Jugador rojo (con brazo izquierdo)</option>
                                                <option value="player-red-with-ball-1">Jugador rojo (con pelota en brazo derecho)</option>
                                                <option value="player-red-with-ball-2">Jugador rojo (con pelota en brazo izquierdo)</option>
                                                <option value="goalkeeper">Portero (con camiseta verde)</option>
                                            </select>
                                        </>
                                    )
                                })
                            }
                            <input type="button" value="Añadir jugador" className="form-teams__add-player" onClick={() => {addPlayer()}}/>
                            <input type="submit" value="Guardar cambios"/>
                            </form>
                        </>
                    :
                        <>
                        <h2 className="config-panel__subtitle">Gestionar posiciones</h2>
                        <p>Seleccione un equipo para editarlo con el botón "Editar equipo"</p>
                        </>
                }
            </div>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        panels: state.panels,
        currentUser: state.currentUser,
        selectedCustomTeam: state.selectedCustomTeam,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        changeBackground: (background) => { dispatch(changeBackground(background)) },
        togglePanel: (panel) => {dispatch(togglePanel(panel))},
        changeObjectSize: (size) => { dispatch(changeObjectSize(size)) },
        addCustomTeam: (selectedTeam, customTeam) => { dispatch(addCustomTeam(selectedTeam, customTeam)) },
        addSelectedCustomTeam: (team) => { dispatch(addSelectedCustomTeam(team)) },
        addMyTeam: (team) => { dispatch(addMyTeam(team)) }
    }
}
 
export default connect(mapStateToProps, mapDispatchToProps)(ConfigPanel);

