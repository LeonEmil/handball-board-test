import React, { useState } from 'react';
import { Link, Redirect } from 'react-router-dom'
import { auth } from '../../firebase/firebase'
import { useFormik } from 'formik'
import * as Yup from 'yup'

const Login = () => {

    const [redirect, setRedirect] = useState(false)

    const { handleSubmit, handleChange, handleBlur, values, errors, touched } = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        validationSchema: Yup.object({
            email: Yup.string().email("Por favor ingrese un email válido").required('Por favor ingrese su email'),
            password: Yup.string().required('Por favor ingrese su contraseña')
        }),
        onSubmit: ({ email, password }) => {
            auth.signInWithEmailAndPassword(email, password)
                .then(() => {
                    setRedirect(true)
                })
                .catch(error => {
                    switch (error.message) {
                        case "There is no user record corresponding to this identifier. The user may have been deleted.":
                            alert("No se encontró al usuario correspondiente a los datos proporcionados. Intentelo nuevamente")
                            break;
                        case "The password is invalid or the user does not have a password.": 
                            alert("La contraseña es incorrecta. Intentelo nuevamente")
                            break;
                        default:
                            alert(error)
                            break;
                    }
                })
        }
    })

    return redirect ? (<Redirect to="/" />) : (
        <div className="login-page">
            <div className="form-image"></div>
            <form className="login__form" onSubmit={handleSubmit}>
                <h1 className="login__title">Ingresar</h1>

                <label htmlFor="email" className="login__label">Email</label>
                <input
                    onChange={handleChange}
                    onBlur={handleBlur}
                    type="email"
                    className="login__input"
                    id="login-email"
                    name="email"
                    value={values.email}
                    required
                />
                {
                    errors.email && touched.email ?
                        <span className="login__alert">{errors.email}</span>
                        : null
                }

                <label htmlFor="password" className="login__label">Contraseña</label>
                <input
                    onChange={handleChange}
                    onBlur={handleBlur}
                    type="password"
                    className="login__input"
                    id="login-password"
                    name="password"
                    value={values.password}
                    required
                />
                {
                    errors.password && touched.password ?
                        <span className="login__alert">{errors.password}</span>
                        : null
                }

                <input
                    type="submit"
                    value="Continuar"
                    className="login__button"
                />
            </form>
            {/* <div className="login__new-container">
                <h2 className="login__new">¿Nuevo usuario?</h2>
                <Link to="/registrarse" className="login__redirect">
                    <button className="login__button">Crear una cuenta nueva</button>
                </Link>
            </div> */}
        </div>
    );
}

export default Login;