import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom'
import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'
import NotFound from './molecules/NotFound'
import './../sass/styles.scss'
import { connect } from 'react-redux'
import { auth, db } from './../firebase/firebase'
import { setUser, changeBackgroundAngle, setConfigData } from './../redux/actionCreators'
//import './../js/functions.js'

class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      width: window.innerWidth,
      height: window.innerHeight
    }
  }

  updateDimensions() {
    if (this.state.width !== window.innerWidth) {
      this.setState({ width: window.innerWidth, height: window.innerHeight });
    } 
  }

  componentDidMount() {
    // Redimensionamiento inicial del canvas
    this.updateDimensions()
    window.addEventListener("resize", this.updateDimensions.bind(this));
    window.addEventListener("orientationchange", this.updateDimensions.bind(this));

    // Configuración del ángulo del fondo de pantalla
    if(this.state.width < this.state.height){
      this.props.changeBackgroundAngle("a_270")
    }
    else {
      this.props.changeBackgroundAngle("a_0")
    }

    //Petición de datos del panel de administración
    db.collection('data').doc('n2lXFZcC0p9aZ4y4BMCE').get().then(response => {
      let configData = {
        bannerAlt: response.data().bannerAlt,
        bannerUrl: response.data().bannerUrl,
        logoUrl: response.data().logoUrl
      }
      this.props.setConfigData(configData)
    })

    // Comprobación y guardado de datos del usuario en el store de redux
    auth.onAuthStateChanged(user => {
      console.log(user)
      if (user) {
        db.collection('users').where('email', '==', `test@email.com`).get()
        .then(response => {
          if(response.docs[0]){
            let currentUser = {
              name: response.docs[0].data().name,
              email: response.docs[0].data().email,
              avatar: response.docs[0].data().avatar,
              strategies: response.docs[0].data().strategies,
              videos: response.docs[0].data().videos,
              customTeams: response.docs[0].data().customTeams,
              otherData: response.docs[0].data().otherData,
              role: response.docs[0].data().role
            }
            this.props.setUser(currentUser)
          }
        })
      }
      else {
        this.props.setUser(user)
      }
    })
  }
  
  componentDidUpdate(){
    // Redimensionamiento del canvas cuando se detecta un redimensionamiento o cambio en la orientación de la pantalla
    this.updateDimensions()
    window.addEventListener("resize", () => {
      this.updateDimensions.bind(this);
      if (this.state.width < this.state.height) {
        this.props.changeBackgroundAngle("a_270")
      }
      else {
        this.props.changeBackgroundAngle("a_0")
      }
    })
    window.addEventListener("orientationchange", (e) => {
      e.preventDefault()
      this.updateDimensions.bind(this)
      if (this.state.width < this.state.height){
        this.props.changeBackgroundAngle("a_270")
      }
      else {
        this.props.changeBackgroundAngle("a_0")
      }
    });

    // Configuración del ángulo del fondo de pantalla
    
    // Comprobación y guardado de datos del usuario en el store de redux
    auth.onAuthStateChanged(user => {
      if (user) {
        db.collection('users').where('email', '==', `test@email.com`).get()
          .then(response => {
              if(response.docs[0]){
                let currentUser = {
                  name: response.docs[0].data().name,
                  email: response.docs[0].data().email,
                  avatar: response.docs[0].data().avatar,
                  strategies: response.docs[0].data().strategies,
                  customTeams: response.docs[0].data().customTeams,
                  videos: response.docs[0].data().videos,
                  otherData: response.docs[0].data().otherData,
                  role: response.docs[0].data().role
                }
                this.props.setUser(currentUser)
              }
          })
      }
      else {
        this.props.setUser(user)
      }
    })
  }
  
  render(){ 
    return (
      <Router basename={process.env.PUBLIC_URL}>  
        <Switch>
          <Route exact path="/">
            <Home canvasWidth={this.state.width} canvasHeight={this.state.height}/>
          </Route>
          <Route component={() => { return <NotFound /> }}></Route>
        </Switch>
      </Router>
    )
  }
};

const mapStateToProps = (state) => {
  return {
    currentUser: state.currentUser,
    currentBackgroundAngle: state.currentBackgroundAngle
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setUser: (currentUser) => { dispatch(setUser(currentUser)) },
    changeBackgroundAngle: (angle) => { dispatch(changeBackgroundAngle(angle)) },
    setConfigData: (data) => { dispatch(setConfigData(data)) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)