import React, { useState, useEffect, useRef } from 'react';
import { Stage, Layer, Group, Shape, Line, Rect, Circle, Image } from 'react-konva';
import Background from './Background'
import useImage from 'use-image' 
import { storage } from './../../firebase/firebase'

import { connect } from 'react-redux'
import DefaultPlayersBlue from './DefaultPlayersBlue';
import DefaultPlayersRed from './DefaultPlayersRed';
import DefaultObjects from './DefaultObjects';
import { createLine, setLine, toggleDrawing, updateObjectPosition } from './../../redux/actionCreators'
//import { drawOn } from "./Menu"


const Canvas = (props) => {

    const [image] = useImage(`https://res.cloudinary.com/leonemil/image/upload/v1602549539/Handball%20manager/preview-2_ecdmqo.jpg`, 'Anonymous')
    const [recicleBynImage] = useImage('https://www.practicepanther.com/wp-content/uploads/2016/07/recycle-bin.png', 'Anonymous')
    
    //console.log(image)

    const [recicleByn, setRecicleByn] = useState(false)

    const [pattern, setPattern] = useState()
    const stageRef = useRef()

    useEffect(() => {
        if(image){
            image.width = window.innerWidth
            image.height = window.innerHeight
        }
        //console.log(image)
    },[image])

    // componentDidUpdate(){
    //     const canvas = document.querySelectorAll('canvas')
    //     console.log(canvas[1])
    //     const context = canvas[1].getContext("2d")
    //     const image = new Image()
    //     image.src = 'https://res.cloudinary.com/leonemil/image/upload/v1602549537/Handball%20manager/preview-5_ylxkus.jpg'
    //     image.width = window.innerWidth
    //     image.height = window.innerHeight
    //     var pattern = context.createPattern(image, 'repeat');
    //     context.rect(0, 0, window.innerWidth, window.innerHeight);
    //     context.fillStyle = "red";
    //     context.fill(); 
    //     console.log(pattern)
    //     setPattern(pattern)
    // }

    // componentDidUpdate(){
    //     const canvas = document.querySelectorAll('canvas')
    //     const c = canvas[1].getContext("2d")
    //     c.fillRect(100, 100, 400, 400)
    //     c.fill()
    // }

    const handleMouseDown = () => {
        if(!props.eraser){
            props.createLine(props.currentLineColor, props.currentLineType)
            props.toggleDrawing(true)
        }
    }
    
    const handleMouseMove = e => {
        if(props.isDrawing && !props.eraser){
            const stage = stageRef.current.getStage();
            const point = stage.getPointerPosition();
            props.setLine(point.x, point.y)
            console.log(props.currentLineColor)
        }
    }
    
    const handleMouseUp = () => {
        props.toggleDrawing(false)
    }

    const arrowSize = props.canvasWidth < 900 || props.canvasHeight < 600 ? 10 : 20
    return (
        <>
        <Stage
            width={props.canvasWidth}
            height={props.canvasHeight}  
            ref={stageRef}
            // onMouseMove={(e) => {
            //     const stage = stageRef.getStage();
            //     const point = stage.getPointerPosition();
            //     setState({
            //         mouseX: point.x,
            //         mouseY: point.y
            //     })
            //     //console.log(state)
            // }}
        >
            <Layer>
                <Background
                    backgroundPath={props.currentBackgroundPath}
                    backgroundAngle={props.currentBackgroundAngle}
                    backgroundImage={props.currentBackgroundCanvas}
                    height={props.canvasHeight}
                    width={props.canvasWidth}
                />  
            </Layer>
            <Layer
                onMouseDown={handleMouseDown}
                onMouseMove={handleMouseMove}
                onMouseUp={handleMouseUp}
                onTouchStart={handleMouseDown}
                onTouchMove={handleMouseMove}
                onTouchEnd={handleMouseUp}
                // onClick={() => {
                //     // const canvas = document.querySelectorAll('canvas')[0]
                //     // const context = canvas.getContext("2d")
                //     // const storageRef = storage.ref()
                //     //     canvas.toBlob(blob => {
                //     //         var image = new Image();
                //     //         image.src = blob;
                //     //         const fileName = "BackgroundPattern"
                //     //         storageRef.child(`${props.currentUser.name}/images/${fileName}`).put(blob)
                //     //             .then(async () => {
                //     //                 let url = await storageRef.child(`${props.currentUser.name}/images/${fileName}`).getDownloadURL()
                //     //                 //var pattern = context.createPattern(url, 'repeat');
                //     //                 //setPattern(pattern)
                //     //                 console.log(url)
                //     //                 //console.log(pattern)
                //     //             })
                //     //             .catch(error => {
                //     //                 console.log(error)
                //     //             });
                //     //     });
                    
                    

                //     //console.log(canvas[1])
                //     //const image = new Image()
                //     //image.src = 'https://res.cloudinary.com/leonemil/image/upload/v1602549537/Handball%20manager/preview-5_ylxkus.jpg'
                //     //image.width = `${window.innerWidth}px`
                //     //image.height = `${window.innerHeight}px`
                //     //console.log(image)
                // //    context.rect(0, 0, window.innerWidth, window.innerHeight);
                // //    context.fillStyle = "red";
                // //    context.fill(); 
                // //    console.log(pattern)
                // }}
            >
                <Background 
                    backgroundPath={props.currentBackgroundPath}
                    backgroundAngle={props.currentBackgroundAngle}
                    backgroundImage={props.currentBackgroundCanvas}
                    height={props.canvasHeight}
                    width={props.canvasWidth}
                />                                     
                
                {
                    props.history[props.historyIndex].lines.map((line, i) => (
                        <Group 
                            key={i} 
                            draggable={props.eraser ? true : false}
                            onDragStart={() => {
                                if (props.eraser) {
                                    setRecicleByn(true)
                                }
                            }}
                            onDragEnd={(e) => {
                                console.log(e.target)
                                let recicleBynPosition = e.target.x() < 1500 && e.target.x() > -1500 &&
                                                         e.target.y() < 1500 && e.target.y() > -1500 ? true : false
                                if (props.eraser && recicleBynPosition) {
                                    e.target.setAttr('x', 10000)
                                }
                                setRecicleByn(false)
                            }}
                            >
                            <Line 
                                points={line.linePoints} 
                                strokeWidth={props.canvasWidth < 900 || props.canvasHeight < 600 ? 2 : 5} 
                                stroke={line.lineColor} 
                                dash={parseInt(line.lineType.slice(-1)) % 2 === 0 ? [10,10] : []} 
                            />
                            <Line 
                                points={line.linePoints} 
                                strokeWidth={25} 
                                stroke="hsla(0, 0%, 100%, 0)" 
                            />
                            <Shape
                                sceneFunc={(context, shape) => {
                                    let startX = line.linePoints[line.linePoints.length - 4] 
                                    let startY = line.linePoints[line.linePoints.length - 3] 
                                    let endX = line.linePoints[line.linePoints.length - 2]
                                    let endY = line.linePoints[line.linePoints.length - 1]
                                    let deltaX = endX - startX
                                    let deltaY = endY - startY
                                    let angle = Math.atan2(deltaY, deltaX)
                                    
                                    context.translate(endX, endY)
                                    context.rotate(angle)
                                    
                                    switch (line.lineType) {
                                        case "linea 3":
                                            context.beginPath();
                                            context.moveTo(0, 0);
                                            context.lineTo(-arrowSize, arrowSize);
                                            context.moveTo(0, 0)
                                            context.lineTo(-arrowSize, -arrowSize);
                                            context.strokeStyle = line.lineColor
                                            context.lineWidth = props.canvasWidth < 900 || props.canvasHeight < 600 ? 2 : 5
                                            context.stroke()
                                            context.closePath();
                                            break;

                                        case "linea 4":
                                            context.beginPath();
                                            context.moveTo(0, 0);
                                            context.lineTo(-arrowSize, arrowSize);
                                            context.moveTo(0, 0)
                                            context.lineTo(-arrowSize, -arrowSize);
                                            context.strokeStyle = line.lineColor
                                            context.lineWidth = props.canvasWidth < 900 || props.canvasHeight < 600 ? 2 : 5
                                            context.stroke()
                                            context.closePath();
                                            break;
                                            
                                        case "linea 5":
                                            context.beginPath();
                                            context.moveTo(0, 0);
                                            context.lineTo(0, -arrowSize);
                                            context.moveTo(0, 0)
                                            context.lineTo(0, arrowSize);
                                            context.strokeStyle = line.lineColor
                                            context.lineWidth = props.canvasWidth < 900 || props.canvasHeight < 600 ? 2 : 5
                                            context.stroke()
                                            context.closePath();
                                            break;
                                            
                                            case "linea 6":
                                                context.beginPath();
                                                context.moveTo(0, 0);
                                                context.lineTo(0, -arrowSize);
                                                context.moveTo(0, 0)
                                                context.lineTo(0, arrowSize);
                                                context.strokeStyle = line.lineColor
                                                context.lineWidth = props.canvasWidth < 900 || props.canvasHeight < 600 ? 2 : 5
                                                context.stroke()
                                            context.closePath();
                                            break;

                                            case "linea 7":
                                                context.beginPath();
                                                context.moveTo(0, 0);
                                            context.lineTo(-arrowSize, -arrowSize);
                                            context.moveTo(0, 0)
                                            context.lineTo(-arrowSize, arrowSize);
                                            context.moveTo(0, 0)
                                            context.lineTo(arrowSize, -arrowSize);
                                            context.moveTo(0, 0)
                                            context.lineTo(arrowSize, arrowSize);
                                            context.strokeStyle = line.lineColor
                                            context.lineWidth = props.canvasWidth < 900 || props.canvasHeight < 600 ? 2 : 5
                                            context.stroke()
                                            context.closePath();
                                            break;

                                        case "linea 8":
                                            context.beginPath();
                                            context.moveTo(0, 0);
                                            context.lineTo(-arrowSize, -arrowSize);
                                            context.moveTo(0, 0)
                                            context.lineTo(-arrowSize, arrowSize);
                                            context.moveTo(0, 0)
                                            context.lineTo(arrowSize, -arrowSize);
                                            context.moveTo(0, 0)
                                            context.lineTo(arrowSize, arrowSize);
                                            context.strokeStyle = line.lineColor
                                            context.lineWidth = props.canvasWidth < 900 || props.canvasHeight < 600 ? 2 : 5
                                            context.stroke()
                                            context.closePath();
                                            break;
                                            
                                            default:
                                            break;
                                    }
                                    
                                    context.rotate(0)
                                    context.translate(0, 0)
                                    context.fillStrokeShape(shape);
                                }}
                                stroke={line.color}
                                strokeWidth={5}
                            />
                        </Group>
                    ))
                }
            </Layer>
            <Layer>
                <DefaultPlayersBlue 
                    history={props.history}
                    historyIndex={props.historyIndex} 
                    toggleDrawing={props.toggleDrawing} 
                    eraser={props.eraser}
                    width={props.canvasWidth} 
                    height={props.canvasHeight}
                    currentObjectSize={props.currentObjectSize}
                    updateObjectPosition={props.updateObjectPosition}  
                    currentBackgroundAngle={props.currentBackgroundAngle} 
                    recicleByn={props.recicleByn}               
                />
                <DefaultPlayersRed 
                    history={props.history}
                    historyIndex={props.historyIndex} 
                    toggleDrawing={props.toggleDrawing}
                    eraser={props.eraser} 
                    width={props.canvasWidth} 
                    height={props.canvasHeight}
                    currentObjectSize={props.currentObjectSize}
                    updateObjectPosition={props.updateObjectPosition}
                    recicleByn={props.recicleByn}
                />
                <DefaultObjects 
                    history={props.history}
                    historyIndex={props.historyIndex}
                    toggleDrawing={props.toggleDrawing} 
                    eraser={props.eraser}
                    width={props.canvasWidth} 
                    height={props.canvasHeight}
                    currentObjectSize={props.currentObjectSize}
                    updateObjectPosition={props.updateObjectPosition}
                    recicleByn={props.recicleByn}
                />
                    {
                        recicleByn ?
                            <>
                                <Rect
                                    x={0}
                                    y={0}
                                    width={window.innerWidth}
                                    height={window.innerHeight}
                                    fill="hsla(0, 0%, 0%, 0.5)"
                                >
                                </Rect>
                                <Circle
                                    radius={200}
                                    x={0}
                                    y={(window.innerHeight / 2)}
                                    fill="hsla(0, 100%, 50%, 0.5)"
                                />
                            </>
                            : null
                    }
            </Layer>
        </Stage>
        </>
    )
}

const mapStateToProps = state => {
    return {
        currentUser: state.currentUser,
        history: state.history,
        historyIndex: state.historyIndex,
        currentObjectSize: state.currentObjectSize,
        currentLineColor: state.currentLineColor,
        currentLineType: state.currentLineType,
        currentBackgroundPath: state.currentBackgroundPath,
        currentBackgroundAngle: state.currentBackgroundAngle,
        currentBackgroundCanvas: state.currentBackgroundCanvas,
        isDrawing: state.isDrawing,
        eraser: state.eraser,
        recicleByn: state.recicleByn
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createLine: (currentLineColor, currentLineType) => { dispatch(createLine(currentLineColor, currentLineType)) },
        setLine: (x, y) => { dispatch(setLine(x, y)) },
        toggleDrawing: (boolean) => { dispatch(toggleDrawing(boolean)) },
        updateObjectPosition: (objectType, typeId, elementId, x, y) => { dispatch(updateObjectPosition(objectType, typeId, elementId, x, y)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Canvas)